module management_backend

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.0
	chainmaker.org/chainmaker/pb-go/v2 v2.3.1
	chainmaker.org/chainmaker/sdk-go/v2 v2.3.1
	chainmaker.org/chainmaker/utils/v2 v2.3.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/emirpasic/gods v1.12.0
	github.com/ethereum/go-ethereum v1.10.4
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/hokaccha/go-prettyjson v0.0.0-20201222001619-a42f9ac2ec8e
	github.com/jinzhu/gorm v1.9.16
	github.com/mojocn/base64Captcha v1.3.4
	github.com/mr-tron/base58 v1.2.0
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
	github.com/tjfoc/gmsm v1.4.1
	go.uber.org/zap v1.17.0
	golang.org/x/text v0.3.6
	gopkg.in/yaml.v2 v2.4.0
	gotest.tools v2.2.0+incompatible
)
